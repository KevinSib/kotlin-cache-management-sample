package com.kevinsibue.kotlin.cachesample.data.repositories

import com.kevinsibue.kotlin.cachesample.data.ApiManager
import com.kevinsibue.kotlin.cachesample.data.remote.CharacterResult
import com.kevinsibue.kotlin.cachesample.data.entity.Character
import io.reactivex.Single

interface ICharacterRepository {
    fun getCharacters(): Single<CharacterResult>
    fun getCharacterById(id: Long): Single<Character>
}

class CharacterRepository(private val apiManager: ApiManager): ICharacterRepository {

    override fun getCharacters(): Single<CharacterResult> = apiManager.retrieveCharacters()

    override fun getCharacterById(id: Long): Single<Character> = apiManager.retrieveDetailCharacter("$id")

}