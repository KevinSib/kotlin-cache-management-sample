package com.kevinsibue.kotlin.cachesample.data

import com.kevinsibue.kotlin.cachesample.data.remote.CharacterResult
import com.kevinsibue.kotlin.cachesample.data.entity.Character
import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

private const val API_BASE_URL = "https://rickandmortyapi.com/"

class ApiManager {

    private val service: ApiService

    interface ApiService {

        @GET("api/character")
        fun retrieveCharacters(): Single<CharacterResult>

        @GET("/api/character/{charactId}")
        fun retrieveDetailCharactere(@Path("charactId") url: String): Single<Character>

    }

    init {
        service = Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(
                OkHttpClient().newBuilder().addInterceptor(HttpLoggingInterceptor().apply { HttpLoggingInterceptor.Level.BASIC })
                    .build()
            ).build()
            .create(ApiService::class.java)
    }

    fun retrieveCharacters() = service.retrieveCharacters()

    fun retrieveDetailCharacter(url: String) = service.retrieveDetailCharactere(url)
}