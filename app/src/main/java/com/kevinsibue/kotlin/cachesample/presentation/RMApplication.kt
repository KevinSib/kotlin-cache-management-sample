package com.kevinsibue.kotlin.cachesample.presentation

import android.app.Application
import com.kevinsibue.kotlin.cachesample.data.ApiManager
import com.kevinsibue.kotlin.cachesample.data.repositories.CharacterCacheRepository
import com.kevinsibue.kotlin.cachesample.data.repositories.CharacterRepository
import com.kevinsibue.kotlin.cachesample.data.repositories.CharacterResultCache
import com.kevinsibue.kotlin.cachesample.data.repositories.ICharacterRepository

class RMApplication : Application() {

    companion object {
        lateinit var app: RMApplication
    }

    lateinit var characterRepository: ICharacterRepository

    override fun onCreate() {
        super.onCreate()
        app = this
        initInjection()
    }

    private fun initInjection() {
        characterRepository =  CharacterCacheRepository(
            CharacterRepository(ApiManager()),
            CharacterResultCache()
        )
    }

}