package com.kevinsibue.kotlin.cachesample.presentation.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.kevinsibue.kotlin.cachesample.R
import com.kevinsibue.kotlin.cachesample.presentation.adapters.BaseRecyclerViewAdapter
import com.kevinsibue.kotlin.cachesample.presentation.viewHolders.BaseViewHolder
import com.kevinsibue.kotlin.cachesample.data.entity.Character
import com.kevinsibue.kotlin.cachesample.presentation.adapters.CharactersRecyclerViewAdapters
import com.kevinsibue.kotlin.cachesample.presentation.viewHolders.ClickHandler
import com.kevinsibue.kotlin.cachesample.presentation.viewModels.CharactersViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BaseRecyclerViewAdapter.IRecyclerViewManager<Character>,
    BaseViewHolder.IItemOnClickListener<Character> {

    protected val viewModel: CharactersViewModel by lazy {
        ViewModelProviders.of(this).get(CharactersViewModel::class.java)
    }

    override val onClickListenerManager: BaseViewHolder.IItemOnClickListener<Character>
        get() = this

    override val items: MutableList<Character>
        get() {
            viewModel.let {
                it.mItems.value?.let {
                    return it
                }
            }
            return mutableListOf()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViewModelObserver()
        lifecycle.addObserver(viewModel)
    }

    override fun onStart() {
        super.onStart()
        activity_characters_recyclerview?.let {

            val adapter = CharactersRecyclerViewAdapters()
            adapter.manager = this

            it.layoutManager = GridLayoutManager(this, 2)
            it.adapter = adapter

        }
        activity_characters_swipe.setOnRefreshListener {
            viewModel.loadData()
        }
    }

    fun initViewModelObserver() {
        viewModel.mItems.observe(this, Observer {
            activity_characters_swipe.isRefreshing = false
            activity_characters_recyclerview?.adapter?.notifyDataSetChanged()
        })
        viewModel.mIsLoading.observe(this, Observer {
            if (it)
                startLoading()
            else
                stopLoading()
        })
    }

    //region RecyclerView Manager Methods

    override fun numberOfItem(): Int = items.size

    override fun getItemAtPosition(position: Int): Character = items[position]

    //endregion

    //region ItemClick Delegate Methods

    override var onClickBlock: ClickHandler<Character> = { obj, _ ->
        //  nothing to do here for the moment!
    }

    //endregion

    //region Loading management

    fun startLoading() {
        activity_character_progressbar.visibility = View.VISIBLE
        activity_character_loading_group.visibility = View.GONE
    }

    fun stopLoading() {
        activity_characters_swipe.isRefreshing = false
        activity_character_progressbar.visibility = View.GONE
        activity_character_loading_group.visibility = View.VISIBLE
    }

    //endregion

}
