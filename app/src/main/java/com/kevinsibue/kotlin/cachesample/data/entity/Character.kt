package com.kevinsibue.kotlin.cachesample.data.entity

data class Character (
    val id: Long,
    val name: String,
    val image: String,
    val species: String
)