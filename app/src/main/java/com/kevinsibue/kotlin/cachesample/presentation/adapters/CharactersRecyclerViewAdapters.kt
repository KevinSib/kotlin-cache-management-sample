package com.kevinsibue.kotlin.cachesample.presentation.adapters

import android.view.ViewGroup
import com.kevinsibue.kotlin.cachesample.R
import com.kevinsibue.kotlin.cachesample.data.entity.Character
import com.kevinsibue.kotlin.cachesample.presentation.viewHolders.BaseViewHolder
import com.kevinsibue.kotlin.cachesample.presentation.viewHolders.CharacterViewHolder

class CharactersRecyclerViewAdapters: BaseRecyclerViewAdapter<Character>() {

    override fun createNewViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Character> {
        inflateViewFromLayout(parent, R.layout.cell_character)?.let {
            return CharacterViewHolder(it)
        }
        return CharacterViewHolder(parent.rootView)
    }

}