package com.kevinsibue.kotlin.cachesample.data.remote

import com.kevinsibue.kotlin.cachesample.data.entity.Info
import com.kevinsibue.kotlin.cachesample.data.entity.Character

data class CharacterResult (
    val info: Info,
    val results: List<Character>
)